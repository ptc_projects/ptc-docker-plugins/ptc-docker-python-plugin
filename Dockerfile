ARG MYAPP_IMAGE=srustem3/ptc_ppa
FROM $MYAPP_IMAGE

WORKDIR /usr/src

COPY . .
RUN pip install . && rm -r /usr/src
COPY resources/tests_setup.cfg /root/.config/flake8
COPY resources/tests_setup.cfg /root/.config/config

ENTRYPOINT ["ptc-run"]
