import setuptools


setuptools.setup(
    name='ptc-docker-python-plugin',
    version='0.0.1',
    description='PTC Docker Plugin',
    author='Rustem Saitgareev',
    author_email='srustem3@yandex.ru',
    packages=setuptools.find_packages(include=['ptc_docker_python_plugin', 'ptc_docker_python_plugin.*']),
    package_data={
        "": ['*.j2']
    },
    install_requires=[
        'ptc-docker-core>=0.0.1'
    ],
    python_requires='>=3.8',
    entry_points={
        'ptc_docker': [
            'ptc_docker_python_plugin = ptc_docker_python_plugin.__main__:main'
        ]
    }
)
