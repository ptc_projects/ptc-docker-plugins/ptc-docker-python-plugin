import os
from typing import Optional, List, Tuple

from ptc_docker_core.constants import SUBMISSION_DIRECTORY_PATH
from ptc_docker_core.executors.tool_executor import ToolExecutor
from ptc_docker_core.loggers.abstract_logger import AbstractLogger
from ptc_docker_core.helper_classes import TaskStatus


class PytestTestsExecutor(ToolExecutor):
    """
    This class is used to run Pytest tests
    """
    def __init__(
            self,
            state_name: str,
            run_cmd: str = "pytest",
            file_extension: str = ".py",
            execution_directory_path: str = SUBMISSION_DIRECTORY_PATH,
            loggers: Optional[List[AbstractLogger]] = None,
            source_code_directory_path: str = SUBMISSION_DIRECTORY_PATH
    ):
        super().__init__(state_name, run_cmd, file_extension, execution_directory_path, loggers)
        self.source_code_directory_path = source_code_directory_path

    def _run_subprocess(self, task_name: str) -> Tuple[TaskStatus, str, int]:
        """
        This method runs Pytest for a given task.
        Adds some preprocessing for ToolExecutor's `_run_subprocess` method and calls it.

        :param task_name: name of task for which Pytest will run
        :return: information about Pytest execution for given task.
        """
        test_path = os.path.join(self.execution_directory_path, f"{task_name}{self.file_extension}")
        source_code_path = os.path.join(self.source_code_directory_path, f"{task_name}{self.file_extension}")
        if not os.path.isfile(test_path):
            return TaskStatus.SKIPPED, "", 0
        if not os.path.isfile(source_code_path):
            return TaskStatus.NOT_SUBMITTED, "", 1
        return super()._run_subprocess(task_name)
