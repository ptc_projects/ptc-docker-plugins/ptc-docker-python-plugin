import importlib.resources as pkg_resources

from ptc_docker_core.loggers.mr_description_logger import MRDescLogger
from ptc_docker_python_plugin.templates import logs as logs_templates


class MRDescLoggerV2(MRDescLogger):
    """
    This class logs provided states to MR description (V2, changed only template to show plugin work)
    """

    def __init__(
            self,
            config_file_path: str,
            template_content: str = pkg_resources.read_text(logs_templates, 'mr_description_log_v2.md.j2')
    ):
        super().__init__(config_file_path, template_content)
