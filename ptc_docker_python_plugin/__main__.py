# No inspection is added because these imports load all classes to ClassesByNamesRegistry
# noinspection PyUnresolvedReferences
from ptc_docker_python_plugin.executors.pytest_executor import PytestTestsExecutor
# noinspection PyUnresolvedReferences
from ptc_docker_python_plugin.loggers.mr_description_logger_v2 import MRDescLogger


def main():
    pass


if __name__ == '__main__':
    main()
